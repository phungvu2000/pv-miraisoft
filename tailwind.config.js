/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      spacing: {
        0.5: '2px',
        0.75: '3px',
        1.5: '0.375rem',
        1.75: '7px',
        2.25: '9px',
        2.5: '10px',
        2.75: '11px',
        3.25: '13px',
        3.75: '15px',
        5.5: '22px',
        6.5: '26px',
        7.5: '30px',
        62: '248px'
      },
      colors: {
        'blue-main': '#356EF1'
      },
      backgroundColor: {
        main: '#FAFAFD'
      },
      fontSize: {
        xxs: ['0.625rem', '1rem']
      }
    }
  },
  plugins: []
};
