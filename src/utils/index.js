export const generateId = () => {
  return 'id-' + Math.random().toString(36).substr(2, 9) + '-' + Date.now().toString(36);
};

export const byteToKilobyte = (data) => {
  return data / 1024;
};
