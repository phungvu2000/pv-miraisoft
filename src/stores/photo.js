import { ref, computed } from 'vue';
import { defineStore } from 'pinia';

export const usePhotos = defineStore('photo', () => {
  const folders = ref([
    {
      id: 1,
      name: 'Uploads',
      children: [
        {
          id: 10,
          name: 'Images',
          children: [
            {
              id: 101,
              name: 'Seasandiego.jpg',
              url: 'https://assets.site-static.com/userFiles/2597/image/2023/CARDIFF_BY_THE_SEA/5-23-2023_1__reasons-why-cardiff-by-the-sea-san-diego-great-place-to-live/9_Reasons_Why_Cardiff-by-the-Sea_San_Diego_.jpg',
              type: 'image/jpg',
              dimmension: '2000x2000',
              size: '763300',
              photo_by: 'Admin'
            },
            {
              id: 102,
              name: 'iMactwin.png',
              url: 'https://i.insider.com/60e4cb0d22d19400191c957c?width=1000&format=jpeg&auto=webp',
              type: 'image/png',
              dimmension: '2000x2000',
              size: '640200',
              photo_by: 'Apple'
            },
            {
              id: 103,
              name: 'hustlecup.jpg',
              url: 'https://images.deliveryhero.io/image/fd-ph/LH/kmph-hero.jpg',
              type: 'image/jpg',
              dimmension: '2000x2000',
              size: '100400',
              photo_by: 'Admin'
            },
            {
              id: 104,
              name: 'MS-Surface.jpg',
              url: 'https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RE4OXzi?ver=3a58',
              type: 'image/jpg',
              dimmension: '2000x2000',
              size: '113200',
              photo_by: 'Admin'
            },
            {
              id: 105,
              name: 'Famoustower.jpg',
              url: 'https://www.worldfamousthings.com/wp-content/uploads/2018/01/Leaning-Tower-of-Pisa-Italy.jpg',
              type: 'image/jpg',
              dimmension: '2000x2000',
              size: '763300',
              photo_by: 'Admin'
            }
          ]
        },
        {
          id: 20,
          name: 'Document',
          children: []
        },
        {
          id: 30,
          name: 'Videos',
          children: [
            {
              id: 301,
              name: 'GODZILLA MINUS ONE Official Trailer',
              url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/330px-No-Image-Placeholder.svg.png?20200912122019',
              type: 'video/mp4',
              dimmension: '2000x2000',
              size: '763300',
              photo_by: 'Admin'
            },
            {
              id: 302,
              name: 'Marvel Studios’ Loki Season 2 | October 6 on Disney+',
              url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/330px-No-Image-Placeholder.svg.png?20200912122019',
              type: 'video/mp4',
              dimmension: '2000x2000',
              size: '763300',
              photo_by: 'Admin'
            },
            {
              id: 303,
              name: 'THE BOY AND THE HERON | Official Teaser Trailer',
              url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/330px-No-Image-Placeholder.svg.png?20200912122019',
              type: 'video/mp4',
              dimmension: '2000x2000',
              size: '763300',
              photo_by: 'Admin'
            }
          ]
        }
      ]
    },
    {
      id: 2,
      name: 'Sources',
      children: []
    },
    {
      id: 3,
      name: 'Shared',
      children: []
    }
  ]);
  const total = computed(() => folders.value);
  const usedMemory = computed(() => computedDataByte(folders.value));
  const photoBy = computed(() => {
    const photoByArray = computedPhotoBy(folders.value);
    let uniq = [...new Set(photoByArray)];
    return uniq;
  });
  const foldersInfo = computed(() => computedFoldersInfo(folders.value));

  function computedDataByte(folder) {
    let storage = 0;
    for (let data of folder) {
      if (!data?.children) {
        if (data?.size) {
          let size = Number(data?.size);
          storage += size ? size : 0;
        }
      } else storage += computedDataByte(data?.children);
    }
    return storage;
  }

  function computedPhotoBy(folder) {
    let newArray = [];
    for (let data of folder) {
      if (!data?.children) {
        if (data?.photo_by) {
          newArray.push(data?.photo_by);
        }
      } else newArray = [...newArray, ...computedPhotoBy(data?.children)];
    }
    return newArray;
  }

  function computedFoldersInfo(folder) {
    let folderInfo = [];
    for (let item of folder) {
      if (item?.children) {
        let info = { name: item?.name };
        folderInfo.push(info);
        const children = computedFoldersInfo(item?.children);
        const amountFile = children.reduce(
          (total, item) => (item?.amount !== undefined ? total + item?.amount : total + 1),
          0
        );
        info.children = [...children];
        info.amount = amountFile;
      } else {
        let fileInfo = { name: item?.name };
        folderInfo.push(fileInfo);
      }
    }
    return folderInfo;
  }

  function findFolderByName(data, folderName) {
    for (const item of data) {
      if (item.name === folderName) {
        return item;
      } else if (item.children && item.children.length > 0) {
        const result = findFolderByName(item.children, folderName);
        if (result) {
          return result;
        }
      }
    }
  }

  function getImagesInFolder(folder, result) {
    for (const child of folder.children) {
      if (!child?.children) result.push(child);
      if (child.children && child.children.length > 0) {
        getImagesInFolder(child, result);
      }
    }
  }

  function getImageInFolderNames(folderName) {
    const folderData = findFolderByName(folders.value, folderName);
    if (folderData) {
      if (folderData?.children) {
        const allChildrenData = [];
        getImagesInFolder(folderData, allChildrenData);
        return allChildrenData;
      } else return [folderData];
    }
    return [];
  }

  return { folders, total, usedMemory, photoBy, foldersInfo, getImageInFolderNames };
});
